﻿namespace ExchangeRates.Client.Tests {
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using ExchangeRates.Client.Models;
    using Moq;
    using NUnit.Framework;

    public class CurrencyExchangeServiceTests {
        private Mock<IExchangeRatesApiClient> _mockClient;
        private CurrencyExchangeService _service;

        [SetUp]
        public void Setup() {
            _mockClient = new Mock<IExchangeRatesApiClient>();
            _service = new CurrencyExchangeService(_mockClient.Object);
        }

        [Test]
        [TestCase("USD", "GBP")]
        [TestCase("USD", "EUR")]
        [TestCase("GBP", "EUR")]
        [TestCase("gbp", "USD")]
        [TestCase("EUR", "usd")]
        [TestCase("eur", "gbp")]
        public async Task GetCurrentExchangeRateAsync_GoodCurrencyCodes_CallsAPI(string currencyCodeFrom, string currencyCodeTo) {
            _mockClient.Setup(s => s.GetCurrentExchangeRateAsync(currencyCodeFrom.ToUpper(), currencyCodeTo.ToUpper())).Returns(Task.FromResult(new ExchangeRateResponse {
                Base = currencyCodeFrom.ToUpper(), Date = DateTime.Today.ToString("yyyy-MM-dd"), Rates = new Dictionary<string, decimal>{ {currencyCodeTo.ToUpper(), 1.7m }
            }}));

            await _service.GetCurrentExchangeRateAsync(currencyCodeFrom, currencyCodeTo);
            _mockClient.Verify(s => s.GetCurrentExchangeRateAsync(currencyCodeFrom.ToUpper(), currencyCodeTo.ToUpper()), Times.Once);
        }

        [Test]
        [TestCase(null, "GBP")]
        [TestCase("USD", null)]
        [TestCase(null, null)]
        [TestCase("", "USD")]
        [TestCase("EUR", "")]
        [TestCase("", "")]
        [TestCase("  ", "USD")]
        [TestCase("EUR", "   ")]
        [TestCase("     ", "      ")]
        public void GetCurrentExchangeRateAsync_NullOrWhitespaceCurrencyCodes_ThrowsArgumentNullException(string currencyCodeFrom, string currencyCodeTo) {
            Assert.ThrowsAsync<ArgumentNullException>(async () => {
                await _service.GetCurrentExchangeRateAsync(currencyCodeFrom, currencyCodeTo);
            });
        }

        [Test]
        [TestCase("USDA", "GBP")]
        [TestCase("USD", "EURAA")]
        [TestCase("GBPA", "EURAA")]
        [TestCase("G", "USD")]
        [TestCase("EUR", "US")]
        [TestCase("E", "GB")]
        public void GetCurrentExchangeRateAsync_InvalidLength_ThrowsArgumentException(string currencyCodeFrom, string currencyCodeTo)
        {
            Assert.ThrowsAsync<ArgumentException>(async () => {
                await _service.GetCurrentExchangeRateAsync(currencyCodeFrom, currencyCodeTo);
            });
        }
    }
}