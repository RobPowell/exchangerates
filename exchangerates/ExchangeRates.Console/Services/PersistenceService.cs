﻿namespace ExchangeRates.Console.Services {
    using System.Threading.Tasks;
    using ExchangeRates.Client.Models;
    using Microsoft.EntityFrameworkCore;

    public class PersistenceService : IPersistenceService {
        private readonly DbContextOptions<ExchangeRatesDbContext> _options;

        public PersistenceService() {
            _options = new DbContextOptionsBuilder<ExchangeRatesDbContext>().UseInMemoryDatabase(databaseName: "ExchangeRates").Options;
        }

        public async Task StoreExchangeRateAsync(ExchangeRate rate) {
            using ExchangeRatesDbContext context = new ExchangeRatesDbContext(_options);
            context.ExchangeRates.Add(rate);
            await context.SaveChangesAsync();
        }
    }
}