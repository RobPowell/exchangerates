﻿namespace ExchangeRates.Console.Services {
    using System;
    using System.Threading.Tasks;
    using ExchangeRates.Client.Models;

    public interface IPersistenceService {
        /// <summary>
        /// Persists the exchange rate to the database.
        /// </summary>
        /// <param name="rate">The data to be stored.</param>
        /// <exception cref="ArgumentException">An argument exception is returned if the key already exists.</exception>
        /// <returns>An async task with no returned data.</returns>
        Task StoreExchangeRateAsync(ExchangeRate rate);
    }
}