﻿namespace ExchangeRates.Console {
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using ExchangeRates.Client;
    using ExchangeRates.Client.Models;
    using ExchangeRates.Console.Services;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Logging;
    using RestSharp;

    public class Program {
        private static ICurrencyExchangeService _currencyExchangeService;
        private static IPersistenceService _dataPersistenceService;

        public static async Task Main(string[] args) {
            Console.WriteLine("Application start");

            // ReSharper disable once RedundantAssignment
            ILogger<ExchangeRatesApiClient> apiClientLogger = null;
#if DEBUG
            using ILoggerFactory loggerFactory = LoggerFactory.Create(builder => builder.AddConsole());
            apiClientLogger = loggerFactory.CreateLogger<ExchangeRatesApiClient>();
#endif

            IConfigurationRoot config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            _currencyExchangeService = new CurrencyExchangeService(new ExchangeRatesApiClient(new RestClient(config.GetValue<string>("ExchangeRatesApi:BaseUrl")), apiClientLogger));
            _dataPersistenceService = new PersistenceService();

            while (true) {
                Console.WriteLine("What is your base currency code?");
                string baseCurrency = Console.ReadLine();

                Console.WriteLine("What is your target currency code?");
                string targetCurrency = Console.ReadLine();

                ExchangeRate result;
                try  {
                    result = await _currencyExchangeService.GetCurrentExchangeRateAsync(baseCurrency, targetCurrency);
                }
                catch (HttpRequestException) {
                    Console.WriteLine($"A currency code was not supported by the server{Environment.NewLine}");
                    continue;
                }
                catch (Exception ex) {
                    if (ex is ArgumentNullException || ex is ArgumentException) {
                        Console.WriteLine($"Invalid currency code entered{Environment.NewLine}");
                        continue;
                    }

                    throw;
                }

                try {
                    await _dataPersistenceService.StoreExchangeRateAsync(result);
                }
                catch (ArgumentException) {
                    Console.WriteLine("The latest combination of these currency codes is already present in the database");
                }

                Console.WriteLine($"The conversion rate is: {result.Rate}{Environment.NewLine}");
            }
        }
    }
}