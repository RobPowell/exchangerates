﻿namespace ExchangeRates.Console {
    using ExchangeRates.Client.Models;
    using Microsoft.EntityFrameworkCore;

    public class ExchangeRatesDbContext : DbContext {
        public ExchangeRatesDbContext(DbContextOptions<ExchangeRatesDbContext> options) : base(options) {
        }

        public DbSet<ExchangeRate> ExchangeRates { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<ExchangeRate>().HasKey(c => new {c.BaseCurrency, c.ComparisonCurrency, c.Date});
        }
    }
}