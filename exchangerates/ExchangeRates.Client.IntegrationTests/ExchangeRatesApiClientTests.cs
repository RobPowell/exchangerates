namespace ExchangeRates.Client.IntegrationTests {
    using System.Net.Http;
    using System.Threading.Tasks;
    using ExchangeRates.Client.Models;
    using Microsoft.Extensions.Configuration;
    using NUnit.Framework;
    using RestSharp;

    public class ExchangeRatesApiClientTests {
        private ExchangeRatesApiClient _client;

        [SetUp]
        public void Setup() {
            IConfigurationRoot config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
            _client = new ExchangeRatesApiClient(new RestClient(config.GetValue<string>("ExchangeRatesApi:BaseUrl")));
        }

        [Test]
        [TestCase("USD", "GBP")]
        [TestCase("USD", "EUR")]
        [TestCase("GBP", "EUR")]
        public async Task GetCurrentExchangeRateAsync_GoodCurrencyCodes_ReturnsDecimalRate(string currencyCodeExchangeFrom, string currencyCodeExchangeTo) {
            ExchangeRateResponse value = await _client.GetCurrentExchangeRateAsync(currencyCodeExchangeFrom, currencyCodeExchangeTo);
            Assert.Pass();
        }

        [Test]
        [TestCase("USD", "ABCD")]
        [TestCase("ABCD", "EUR")]
        [TestCase("ABCD", "ABCD")]
        public void GetCurrentExchangeRateAsync_BadCurrencyCodes_ReturnsHttpRequestExceptionCodeBadRequest(string currencyCodeExchangeFrom, string currencyCodeExchangeTo) {
            HttpRequestException ex = Assert.ThrowsAsync<HttpRequestException>(async () => {
                ExchangeRateResponse value = await _client.GetCurrentExchangeRateAsync(currencyCodeExchangeFrom, currencyCodeExchangeTo);
            });

            Assert.That(ex.Message, Is.EqualTo($"Couldn't process GET from https://api.exchangeratesapi.io/latest?base={currencyCodeExchangeFrom}&symbols={currencyCodeExchangeTo}. HTTP result was BadRequest"));
        }
    }
}