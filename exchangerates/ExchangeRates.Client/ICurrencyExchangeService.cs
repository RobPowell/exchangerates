﻿namespace ExchangeRates.Client {
    using System;
    using System.Net.Http;
    using System.Threading.Tasks;
    using ExchangeRates.Client.Models;

    public interface ICurrencyExchangeService {
        /// <summary>
        /// Retrieves the exchange rate for two currency codes.
        /// </summary>
        /// <param name="currencyCodeExchangeFrom">The base currency code to exchange from as an ISO 4217 code.</param>
        /// <param name="currencyCodeExchangeTo">The base currency code to exchange to as an ISO 4217 code.</param>
        /// <exception cref="HttpRequestException">The API rejected the requested currency codes as unrecognised or unsupported.</exception>
        /// <exception cref="ArgumentNullException">The currency code was null or whitespace.</exception>
        /// <exception cref="ArgumentException">The currency code was not the correct length to be a valid code.</exception>
        /// <returns>An <see cref="ExchangeRate"/> representing the rate of exchange between the two currency codes on the latest date.</returns>
        Task<ExchangeRate> GetCurrentExchangeRateAsync(string currencyCodeExchangeFrom, string currencyCodeExchangeTo);
    }
}