﻿namespace ExchangeRates.Client.Models {
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class ExchangeRate {
        [Key, Column(Order = 0)]
        public string BaseCurrency { get; set; }

        [Key, Column(Order = 1)]
        public string ComparisonCurrency { get; set; }

        public decimal Rate { get; set; }

        [Key, Column(Order = 2)]
        public DateTime Date { get; set; }
    }
}