﻿namespace ExchangeRates.Client.Models {
    using System.Collections.Generic;

    public class ExchangeRateResponse {
        public Dictionary<string, decimal> Rates { get; set; }
        public string Base { get; set; }
        public string Date { get; set; }
    }
}