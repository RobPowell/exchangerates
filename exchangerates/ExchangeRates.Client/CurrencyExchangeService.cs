﻿namespace ExchangeRates.Client {
    using System;
    using System.Threading.Tasks;
    using ExchangeRates.Client.Models;

    public class CurrencyExchangeService : ICurrencyExchangeService {
        private readonly IExchangeRatesApiClient _client;

        public CurrencyExchangeService(IExchangeRatesApiClient client) {
            _client = client;
        }

        public async Task<ExchangeRate> GetCurrentExchangeRateAsync(string currencyCodeExchangeFrom, string currencyCodeExchangeTo) {
            if (string.IsNullOrWhiteSpace(currencyCodeExchangeFrom)) {
                throw new ArgumentNullException(nameof(currencyCodeExchangeFrom));
            }

            if (string.IsNullOrWhiteSpace(currencyCodeExchangeTo)) {
                throw new ArgumentNullException(nameof(currencyCodeExchangeTo));
            }

            if (currencyCodeExchangeFrom.Length != 3) {
                throw new ArgumentException("Currency codes should be 3 characters", nameof(currencyCodeExchangeFrom));
            }

            if (currencyCodeExchangeTo.Length != 3) {
                throw new ArgumentException("Currency codes should be 3 characters", nameof(currencyCodeExchangeTo));
            }

            // API only accepts capitals
            currencyCodeExchangeFrom = currencyCodeExchangeFrom?.ToUpper();
            currencyCodeExchangeTo = currencyCodeExchangeTo?.ToUpper();

            ExchangeRateResponse apiResponse = await _client.GetCurrentExchangeRateAsync(currencyCodeExchangeFrom, currencyCodeExchangeTo);
            ExchangeRate rate = new ExchangeRate { BaseCurrency = currencyCodeExchangeFrom, ComparisonCurrency = currencyCodeExchangeTo, Rate = apiResponse.Rates[currencyCodeExchangeTo], Date = Convert.ToDateTime(apiResponse.Date) };
            return rate;
        }
    }
}