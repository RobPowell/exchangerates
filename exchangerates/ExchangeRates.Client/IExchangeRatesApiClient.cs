﻿namespace ExchangeRates.Client {
    using System.Threading.Tasks;
    using ExchangeRates.Client.Models;

    public interface IExchangeRatesApiClient {
        Task<ExchangeRateResponse> GetCurrentExchangeRateAsync(string currencyCodeExchangeFrom, params string[] currencyCodesExchangeTo);
    }
}