﻿namespace ExchangeRates.Client {
    using System.Collections.Generic;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using ExchangeRates.Client.Models;
    using Microsoft.Extensions.Logging;
    using RestSharp;

    public class ExchangeRatesApiClient : IExchangeRatesApiClient {
        private readonly IRestClient _restClient;
        private readonly ILogger<ExchangeRatesApiClient> _logger;

        public ExchangeRatesApiClient(IRestClient restClient, ILogger<ExchangeRatesApiClient> logger = null) {
            _restClient = restClient;
            _logger = logger;
        }

        public async Task<ExchangeRateResponse> GetCurrentExchangeRateAsync(string currencyCodeExchangeFrom, params string[] currencyCodesToExchangeTo) {
            RestRequest restRequest = new RestRequest("latest", Method.GET);

            restRequest.AddParameter("base", currencyCodeExchangeFrom, ParameterType.QueryString);
            restRequest.AddParameter("symbols", string.Join(',', currencyCodesToExchangeTo), ParameterType.QueryString);

            return await CallToApiAsync<ExchangeRateResponse>(restRequest);
        }

        private async Task<T> CallToApiAsync<T>(IRestRequest restRequest, int timeout = 120000, ICollection<HttpStatusCode> additionalAcceptedResult = null) where T : new() {
            _logger?.LogInformation($"Call on {_restClient.BaseUrl}{restRequest.Resource} with parameters {string.Join(", ", restRequest.Parameters)}");

            restRequest.Timeout = timeout;

            restRequest.AddHeader("Accept", "application/json");
            IRestResponse<T> restResult = await _restClient.ExecuteAsync<T>(restRequest);

            _logger?.LogInformation($"Response from {_restClient.BaseUrl}{restRequest.Resource} with parameters {string.Join(", ", restRequest.Parameters)}; Status code {restResult.StatusCode}'");
            if (restResult.StatusCode == HttpStatusCode.OK || additionalAcceptedResult != null && additionalAcceptedResult.Contains(restResult.StatusCode)) {
                return restResult.Data;
            }

            throw new HttpRequestException($"Couldn't process {restRequest.Method} from {restResult.ResponseUri}. HTTP result was {restResult.StatusCode}", restResult.ErrorException);
        }
    }
}