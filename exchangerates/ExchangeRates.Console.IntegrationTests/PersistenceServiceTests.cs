namespace ExchangeRates.Console.IntegrationTests {
    using System;
    using System.Threading.Tasks;
    using ExchangeRates.Client.Models;
    using ExchangeRates.Console.Services;
    using NUnit.Framework;

    public class PersistenceServiceTests {
        private PersistenceService _service;

        [SetUp]
        public void Setup() {
            _service = new PersistenceService();
        }
        
        [Test, Order(1)]
        public async Task StoreExchangeRateAsync_ExchangeRateKeyDoesNotExist_StoresSuccessfully() {
            ExchangeRate rate = new ExchangeRate{ BaseCurrency = "GBP", ComparisonCurrency = "USD", Date = DateTime.Today, Rate = 1.7m };

            await _service.StoreExchangeRateAsync(rate);

            Assert.Pass();
        }

        [Test, Order(2)]
        public void StoreExchangeRateAsync_ExchangeRateKeyDoesExist_ThrowsArgumentException() {
            ExchangeRate rate = new ExchangeRate { BaseCurrency = "GBP", ComparisonCurrency = "USD", Date = DateTime.Today, Rate = 1.7m };
            
            ArgumentException ex = Assert.ThrowsAsync<ArgumentException>(async () => {
                await _service.StoreExchangeRateAsync(rate);
            });

            Assert.That(ex.Message, Is.EqualTo("An item with the same key has already been added. Key: System.Object[]"));
        }
    }
}